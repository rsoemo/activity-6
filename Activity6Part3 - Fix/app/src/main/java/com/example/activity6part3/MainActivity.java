package com.example.activity6part3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    Button btn_Add;
    EditText et_EnterName, et_EnterAge;
    ListView lv_NameList;

    List<String> friends = new ArrayList<String>();
    String[] startingList = {"Charlie","Rex","Zach"};

    ArrayAdapter ad;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_Add = findViewById(R.id.btn_Add);
        et_EnterName = findViewById(R.id.et_AddName);
        lv_NameList = findViewById(R.id.lv_NameList);

        friends = new ArrayList<String>(Arrays.asList(startingList));
        ad = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friends);
        lv_NameList.setAdapter(ad);

        btn_Add.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String newName = et_EnterName.getText().toString();
                friends.add(newName);
                Collections.sort(friends);

                ad.notifyDataSetChanged();
            }
        });

        lv_NameList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Toast.makeText(MainActivity.this, "pos=" + position + " name=" + friends.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }
}