package com.example.activity6part1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button btn_Spin;
    ImageView iv_Arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_Spin = (Button) findViewById(R.id.btn_Spin);
        iv_Arrow = (ImageView) findViewById(R.id.iv_Arrow);

        btn_Spin.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int spinDegrees;
                Random r = new Random();
                spinDegrees = r.nextInt(3600);

                RotateAnimation rotateArrow = new RotateAnimation(
                        0, spinDegrees,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f);

                rotateArrow.setDuration(2000);
                rotateArrow.setFillAfter(true);
                rotateArrow.setInterpolator(new AccelerateDecelerateInterpolator());

                iv_Arrow.startAnimation(rotateArrow);

                rotateArrow.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        if (spinDegrees % 360 > 180)
                        {
                            Toast.makeText(MainActivity.this, "The left has been chosen", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(MainActivity.this, "The right has been chosen", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });


            }
        });
    }


}